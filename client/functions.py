import requests


server_adress = "http://127.0.0.1:8000"

class ClientFunctions:

    # Création de deck
    def deck_creation():
        return requests.get(server_adress+"/creer-un-deck")

    #  Tirage de cartes
    def cards_drawing(nb_cards):
        response = requests.get(server_adress+"/cartes/"+str(nb_cards)).json()['cards']
        return [{'image': card['image'], 'value': card['value'], 'suit': card['suit'], 'code': card['code']} for card in response]

    #  Comptage de cartes
    def comptage(cards):
        list_of_cards = {"H": 0, "S": 0, "D": 0, "C": 0}

        for card in cards:
            list_of_cards[card["suit"][0]] += 1

        return list_of_cards


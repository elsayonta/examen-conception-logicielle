# Client
Après installation des dépendances, on peut soit
- exécuter le scénario (qui tire dix cartes), 
- ouvrir l'application qui permet de tirer un nombre de cartes au choix
- exécuter le test 
## Installation de dépendances
`pip install -r requirements.txt`
## Lancement du scénario
`python scenario.py`

Cet exécutable `scenario.py` permet de créer un deck, tirer dix cartes et compter le nombre de cartes par motif ou couleur.

## Lancement de l'application
`python main.py`

## Lancement du test
`python test.py`

Le fichier `functions.py` contient les fonctions utilisées par le client.

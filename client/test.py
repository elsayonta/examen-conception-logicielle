from functions import ClientFunctions


def test_comptage():

    #  GIVEN
    cards = [{'image': 'https://deckofcardsapi.com/static/img/0C.png', 'value': '10', 'suit': 'CLUBS', 'code': '0C'},
            {'image': 'https://deckofcardsapi.com/static/img/9D.png', 'value': '9', 'suit': 'DIAMONDS', 'code': '9D'},
            {'image': 'https://deckofcardsapi.com/static/img/JS.png', 'value': 'JACK', 'suit': 'SPADES', 'code': 'JS'},
            {'image': 'https://deckofcardsapi.com/static/img/0S.png', 'value': '10', 'suit': 'SPADES', 'code': '0S'},
            {'image': 'https://deckofcardsapi.com/static/img/6H.png', 'value': '6', 'suit': 'HEARTS', 'code': '6H'},
            {'image': 'https://deckofcardsapi.com/static/img/aceDiamonds.png', 'value': 'ACE', 'suit': 'DIAMONDS', 'code': 'AD'}]
    expected = {'H': 1, 'S': 2, 'D': 2, 'C': 1}
    #  WHEN
    obtained = ClientFunctions.comptage(cards)
    # THEN
    assert expected == obtained


if __name__ == "__main__":
    test_comptage()

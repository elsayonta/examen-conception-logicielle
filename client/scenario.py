from functions import ClientFunctions

if __name__ == "__main__":
    print('')
    print('')
    print('**********************************************************************')
    print('******************      Exécution du scénario      *******************')
    print('**********************************************************************')
    print('')
    print("Initialisation d'un nouveau jeu de cartes...........")
    print("L'identifiant du paquet de cartes est: ",\
        ClientFunctions.deck_creation().json()['deck id'])
    print('')
    print("Les cartes tirées sont:")
    print('')
    cards = ClientFunctions.cards_drawing(10)

    i=0
    for c in cards:
        i += 1
        print("carte ", i,": ", c)
        print('')
    print('')
    print('Décompte de cartes...................')
    print("Nombre de cartes par motif : ")
    print(ClientFunctions.comptage(cards))
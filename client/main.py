from functions import ClientFunctions
from PyInquirer import prompt


#  Pour le menu
class WelcomeEndView:

    def __init__(self):
        self.questions = [
            {
                'type': "list",
                'name': 'choice',
                'message': "Que voulez-vous faire ?",
                'choices': ["> Initialiser un nouveau jeu de cartes",
                            "> Tirer des cartes",
                            "> Quitter"]
            }
        ]
        self.status = True

    def make_choice(self):

        answer = prompt(self.questions)
        print('')
        if answer['choice'] == "> Initialiser un nouveau jeu de cartes":
            print('')
            print("Initialisation d'un nouveau jeu de cartes...........")
            print("L'identifiant du paquet de cartes est: ",\
                ClientFunctions.deck_creation().json()['deck id'])
            print('')

        elif answer['choice'] == "> Tirer des cartes":

            status2 = True
            while (status2):
                print('')
                print("Choisissez un nombre entre 1 et 52")
                nb_cards = int(input())
                while(nb_cards > 52 or nb_cards < 1): #  Ici on impose l'utilisateur à entrer un entier compris entre 1 et 52
                    print('')
                    print("Veuillez choisir un nombre entre 1 et 52")
                    nb_cards = int(input())

                cards = ClientFunctions.cards_drawing(nb_cards)
                print('')
                print('Les cartes tirées sont :')
                i=0
                for c in cards:
                    i += 1
                    print("carte ", i,": ", c)
                    print('')
                print('')

                print("Voulez-vous compter le nombre de cartes par motif ? (o/n)")
                answer = input()
                if answer == "o":
                    print('')
                    print('Décompte de cartes...................')
                    print("Nombre de cartes par motif : ", ClientFunctions.comptage(cards))
                    print('')

                print("Voulez-vous tirer d'autres cartes dans le même jeu de cartes ? (o/n)")
                answer = input()
                if answer == "n":
                    status2=False



        elif answer['choice'] == "> Quitter":
            print('')
            print("*************************************************************")
            print("************* Merci d'avoir utilisé cette appli *************")
            print("*************            A bientôt!             *************")
            print("*************************************************************")
            print('')
            self.status = False


if __name__ == "__main__":

    print('')
    print('**********************************************************************')
    print('****************   Bienvenue dans mon application   ******************')
    print('**********************************************************************')
    print('')

    execute = WelcomeEndView()
    while(execute.status):
        execute.make_choice()

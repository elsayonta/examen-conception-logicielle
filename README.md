# Examen de conception logicielle 
TP noté de conception logicielle
Le principe de ce TP repose sur l'utilisation de l'api documentée ici:
https://deckofcardsapi.com/
Il s'agit entre autres de créer un webservice qui est un client web de l'api et un client qui requête ce webservice.

## Partie Webservice
Création de deck et tirage de cartes

## Partie Client
En tant que client, il est possible de 
- créer un deck
- tirer des cartes d'un deck en cours
- calculer pour une liste de cartes donnée le nombre de cartes de chaque couleur ou motif sous forme d'un dictionnaire.

## Quickstart
Après avoir cloné le projet, lancer ces commandes:
1. Accès à la racine du projet : `cd examen-conception-logicielle`

2. Accès au webservice: `cd webservice`

3. Installation des dépendances du webservice: `pip3 install -r requirements.txt`

4. Lancement du webservice : `python3 main.py`

Ouvrir un autre terminal, puis:

5. Accès à la racine du projet : `cd examen-conception-logicielle`

6. Accès au client: `cd client`

7. Installation des dépendances du webservice: `pip3 install -r requirements.txt`

8. Lancement de l'appli ou du scénario: `python3 main.py` ou `python3 scenario.py` respectivement

## Lancement des tests
le test implémenté est celui de la fonction de comptage des cartes par motif. 
Pour cela, exécuter ces commandes:

- `cd examen-conception-logicielle`
- `cd client`
- `python3 test.py`

**NB:** En fonction de l'IDE utilisé, l'exécution de tests pourrait ne rien renvoyer, car les tests passent 


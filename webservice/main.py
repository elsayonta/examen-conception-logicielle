import requests
import uvicorn
from fastapi import FastAPI

app = FastAPI()

deck_id = " "
remaining_cards = 0


@app.get("/")
def read_root():
    return {"Welcome to our cards small app"}


# Pour créer un deck
@app.get("/creer-un-deck")
def deck_creation():  
    global deck_id
    global remaining_cards

    request = requests.get("https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1")
    response = request.json()
    deck_id = response["deck_id"]
    remaining_cards = response["remaining"]
    return {"deck id": deck_id}

#  Tirage de cartes
@app.get("/cartes/{nb_cards}")
def cards_drawing(nb_cards: int):
    global deck_id
    global remaining_cards

    if (deck_id == " "):
        request = requests.get("https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1")
        deck_id = request.json()["deck_id"]

    #  cas où le nombre de cartes est négatif ou supérieur à 52

    elif (nb_cards <0 or nb_cards > 52):
        return (" Entrez un nombe compris entre 1 et 52")

    #  Cas limite, où le nombre de cartes demandées est
    #  inférieur aux cartes restantes
    elif(remaining_cards < nb_cards): 
        request = requests.get("https://deckofcardsapi.com/api/deck/"+deck_id+"/shuffle/")
    request = requests.get("https://deckofcardsapi.com/api/deck/"+deck_id+"/draw/?count="+str(nb_cards))
    response = request.json()
    remaining_cards = response["remaining"]
    result = {"deck_id": deck_id, "cards": response["cards"]}

    return result


if __name__ == "__main__":

    uvicorn.run(app, host="127.0.0.1", port=8000)

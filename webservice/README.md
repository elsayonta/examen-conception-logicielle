# Webservice
Le webservice permet de requêter l'api à l'adresse locale (ou de l'IDE utilisé) http://127.0.0.1:8000. Plus précisément: 
- l'URL "http://127.0.0.1:8000/creer-un-deck/" crée un deck et affiche son identifiant
- l'URL "http://127.0.0.1:8000/cartes/{nb_cards}" tire  un nombre *nb_cards* de cartes du deck en cours.

Une fois dans le dossier webservice:
## Installation de dépendances
`pip install -r requirements.txt`
## Lancement de l'application
`python3 main.py`

Ceci nous renvoie sur l'adresse locale http://127.0.0.1:8000.
On peut ainsi saisir l'une des deux URL mentionnées plus haut. 

